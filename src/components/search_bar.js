import React, { Component } from 'react'; //varable Component from react is now defined here

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { term: '' };
  }


  render() {
    return (
      <div className="search-bar">
        <input
          value={this.state.term}
          onChange={event => this.onInputChange(event.target.value)} />
      </div>
    );
  }

  onInputChange(term) {
    this.setState({term});
    this.props.onSearchTermChange(term);
  }
}
// functional component example
// const SearchBar = () => {
//   return <input />;
// }

export default SearchBar;
